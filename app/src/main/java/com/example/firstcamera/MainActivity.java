package com.example.firstcamera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button button;
    Bitmap b;

    int flag=0; // for saving phtoo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.my_image);
        button = (Button) findViewById(R.id.take_photo);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                // opening camera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // 1 - request code
                startActivityForResult(intent, 1); // 1 just a number
                */
                // check
                if (flag == 0) {
                    // taking photo
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 99); // 99 just a number
                } else if (flag == 1) {

                    // code for saving photo

                    saveToSDCARD(b);
                    Toast.makeText(getApplicationContext(), "saved ", Toast.LENGTH_LONG).show();

                    flag = 0;
                    button.setText("Take Photo");
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // checking
        if (requestCode == 99 && resultCode == RESULT_OK && data != null) {
            b = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(b);

            flag = 1; // global ver
            button.setText("Save Photo");

        }
    }

    private void saveToSDCARD(Bitmap bitmap) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyMMdd_HH_mm_ss");
        String p_name = sdf.format(new Date());

        // creating file
        String root = Environment.getExternalStorageDirectory().toString();
        // location
        File folder = new File(root+"/cool_camera");
        folder.mkdirs();

        File my_file = new File(folder, p_name+".png");

        // saving
        try {
            FileOutputStream stream = new FileOutputStream(my_file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
            stream.flush();
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}